
This is a Google Checkout payment module for Drupal's e-Commerce
package.

You must install the Google Checkout PHP library, available from:
http://code.google.com/p/google-checkout-php-sample-code/

Just download the google-checkout-php-sample-code-v1.2.zip file and move
the 'library' directory into the gcheckout module directory:

i.e. /drupal-root/sites/all/modules/gcheckout/library/googlecart.php,
etc.

NON-PROFIT DONATION SUPPORT

Google Checkout's non-profit donation service actually uses a different
URL from normal merchant checkout.  Thus, to simplify the codebase, we
bypass the normal Drupal cart and submit a "Donate" form directly to
Google Checkout, creating a Drupal e-Commerce transaction later.

To implement this, you must create a "Donate Now" button on the Google
Checkout site and, with the "donate" module enabled, create a Donate
product node on your site.  Associate the button with the donate product
by adding some extra HTML to the donate button code: <input
name="item_merchant_id_1" type="hidden" value="NID" /> where NID is the
node id of a "Donate" product node.  When the Donate Now button is
clicked and checkout is completed, Google Checkout will create a
transaction on your Drupal e-Commerce site containing this Donate
product, set to whatever price the donor chose on the form.  Within a 
minute or so the transaction should update to payment completed.
